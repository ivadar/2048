#include "dialog.h"
#include "ui_dialog.h"
#include <QMessageBox>
#include <QFile>
#include <QTextStream>



Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}




void Dialog::on_buttonBox_accepted()
{
    QString name;
    QString file_with_name = "/home/dasha/2048_5/data/bestscore.txt";
    QFile file(file_with_name);
    if (file.open(QIODevice::WriteOnly)) {
        QTextStream out(&file); // поток записываемых данных направляем в файл
        // Для записи данных в файл используем оператор <<
        if (ui->buttonBox->Ok){
            name = ui->name->text();
            out << name << ":";
        }
    }
     file.close();

}
