#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dialog.h"
#include "result.h"
#include <QMessageBox>
#include <QApplication>
#include <QFile>
#include <QTextStream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionAbout_triggered()
{
    QMessageBox:: information(this, "About", "Курсовую работу выполнила: Иванченко Дарья Владимировна. Группа: М8О-110Б-20.");
}



void MainWindow::on_actionRules_triggered()
{
    QMessageBox:: information(this, "Rules", "2048 — это игра, где отлично воссоединились традиционный тетрис и пятнашки."
"Стандартное игровое поле имеет форму квадрата 4x4. Вы можете выбрать размер поля (по умолчанию стоит 4х4). "
"В начале игры вам выдается два кирпичика с цифрой «2», нажимая кнопку вверх, вправо, влево или вниз, все ваши кирпичики будут смещаться в ту же сторону. "
"Но, при соприкосновении клеточек с одним и тем же номиналом, они объединяются и создают сумму вдвое больше. "
"Игра заканчивается тогда, когда все пустые ячейки заполнены, и вы больше не можете передвигать клеточки ни в одну из сторон или когда на одном из кубов, появилась заветная цифра 2048. "
"Удачи!");
}

void MainWindow::on_actionQuit_triggered()
{
    QApplication::quit();
}

void MainWindow::startGame() {
    int selectedSize = ui->comboBox->currentText().toInt();
    GameWindow* window = new GameWindow(0, selectedSize);
    window->show();
    this->hide();
}

void MainWindow::on_pushButton_clicked()
{
    QObject::connect(ui->pushButton, &QPushButton::clicked, this, &MainWindow::startGame);
}




void MainWindow::on_pushButton_2_clicked()
{
    Dialog what_your_name;
    what_your_name.setModal(true);
    what_your_name.exec();
}


void MainWindow::on_pushButton_3_clicked()
{
    Result result;
    result.setModal(true);
    result.exec();

}

