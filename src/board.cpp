#include "board.h"
#include "tile.h"
#include <QtWidgets>
#include <QHBoxLayout>
#include <QVBoxLayout>

void Board::setupBoard() {
    score = 0;
	QGridLayout* layout = new QGridLayout;
    QLabel* labela = new QLabel("Score: " + QString::number(score));
        QLabel* labelb = new QLabel("Best Score: " + QString::number(bestscore));

//    scene = new QGraphicsScene(0, 0, 500, 500);
//    layout->addWidget(lScore = new QLabel(tr("Score:")));
//    layout->addWidget(lScoreBest = new QLabel(tr("Best Score:")));
//    layout->addWidget(pbRestart = new QPushButton("&Restart"));
//    layout->addWidget(pbQuit = new QPushButton("&Quit"));
	for (int i = 0; i < this->boardSize * this->boardSize; i++) {
		Tile* tile = new Tile();
//#define DEBUG_BOARD  // DEBUG_BOARD is for testing colors.
#ifdef DEBUG_BOARD
		if (i == 0) {
			tile->setValue(2);
		} else if (i < 16) {
			tile->addTile(this->tiles[i - 1]);
			tile->addTile(this->tiles[i - 1]);
		}
#endif
		tile->setFixedSize(this->boardScale, this->boardScale);
        layout->addWidget(tile, i / this->boardSize, i % this->boardSize);
		this->tiles << tile;
	}
    layout->addWidget(labela);
        layout->addWidget(labelb);

	this->setLayout(layout);

    connect(tile, SIGNAL(scoreChanged(int)), this, SLOT(scoreUpdate(int)));

    emit scoreChanged(score);


    emit scoreChanged(score);
#ifndef DEBUG_BOARD
	this->afterMovement();
#endif
}

void Board::scoreUpdate(int score)
{
    if (bestscore < score){
        bestscore = score;
    }
    lScore->setText(QString(tr("Score:\n%1")).arg(score));
    lScoreBest->setText(QString(tr("Best Score:\n%1")).arg(bestscore));
}

void Board::resetBoard() {
    foreach (Tile* tile, this->tiles) { // итерация по массиву tiles
        tile->setValue(); // получаем доступ к элементу массива в этой итерации через переменную tile
	}
	this->addRandomTile();
}


void Board::afterMovement() {
    if (this->moved) {                  // если данная ячейка перемещена
        this->addRandomTile();          // добавляем рандомную ячейку
        this->moved = false;            // moved = false, то есть ячейка не перемещали
        if (!this->isMovePossible()) {  // если нет возможности переместить ячейки, то есть нет больше ходов, то
            this->showLoosePopUp();     // вывод сообщения о завершении игры
		}
	}
}


void Board::showLoosePopUp() {
    int popup = QMessageBox::information(this, "Game Over", "You Lost!", QMessageBox::Ok);
//    QString name;
//    QString file_with_name = "/home/dasha/2048_5/data/bestscore.txt";
//    QFile file(file_with_name);
//    if (file.open(QIODevice::WriteOnly)) {
//        QTextStream out(&file); // поток записываемых данных направляем в файл
//        // Для записи данных в файл используем оператор <<
//            out << bestscore << Qt::endl;
//    file.close();

	switch (popup) {
        case QMessageBox::Ok:       // при нажатии Ok игра будет перезапущена
			this->resetBoard();
			break;
		default:
			break;
	}
}


void Board::moveTile(int row, int col, int rowChange, int colChange) {
    Tile* tile = this->tile(row, col);      // получаем элемент массива
    if (tile && !tile->isFree()) {          // если ячейка есть и она не нулевая
        Tile* nextTile = this->tile(row + rowChange, col + colChange);  // получаем элемент, который будет следующим, после текущего
		if (nextTile) {
            if (tile->equalTo(nextTile) || nextTile->isFree()) {    // если текущий элемент равен последующему, то
                nextTile->addTile(tile);                            // добавляем элемент в последующую ячейку
                tile->setValue();                                   // в текущую ячейку будет установлен 0
                this->moved = true;                                 // переменная moved = true, что бы потом можно было добавить рандомную ячейку
                score += nextTile->tileValue;
//                emit scoreChanged(score);
            }
            this->moveTile(row + rowChange, col + colChange, rowChange, colChange); // повторный вызов метода, что бы переместить ячейку до конца
		}
	}
}


void Board::moveUp() {  // перемещение вверх
    for (int r = 1; r < this->boardSize; r++) {     // начиная с 1-го ряда
        for (int c = 0; c < this->boardSize; c++) { // и нулевого столбца
            this->moveTile(r, c, -1, 0);            // вызывается функция перемещения
		}
	}
    this->afterMovement(); // вызывается функция, которая проверит есть ли возможность добавить элемент или переместить уже существующие
}


void Board::moveDown() {    // перемещение вниз
    for (int r = this->boardSize; r >= 0; r--) {    // начиная с нижнего ряда
        for (int c = 0; c < this->boardSize; c++) { // и нулевого столбца
            this->moveTile(r, c, 1, 0);             // вызывается функция перемещения
		}
	}
    this->afterMovement(); // вызывается функция, которая проверит есть ли возможность добавить элемент или переместить уже существующие
}


void Board::moveLeft() {    // перемещение влево
    for (int r = 0; r < this->boardSize; r++) {     // начиная с нулевого ряда
        for (int c = 1; c < this->boardSize; c++) { // и 1-го стобца
            this->moveTile(r, c, 0, -1);            // вызывается функция перемещения
		}
	}
    this->afterMovement(); // вызывается функция, которая проверит есть ли возможность добавить элемент или переместить уже существующие
}


void Board::moveRight() {   // перемещение вправо
    for (int r = 0; r < this->boardSize; r++) {      // начиная с нулевого ряда
        for (int c = this->boardSize; c >= 0; c--) { // и с крайнего столбца
            this->moveTile(r, c, 0, 1);              // вызывается функция перемещения
		}
	}
    this->afterMovement();// вызывается функция, которая проверит есть ли возможность добавить элемент или переместить уже существующие
}


Tile* Board::tile(int row, int col) { // установка соответсвия элементов массива и ячеек
    if (this->inRange(row, col)) {  // если элемент входит в диапозон, то
        return this->tiles[this->boardSize * row + col]; // кладем его в массив tiles с порядковым номером
	}
	return nullptr;
}


bool Board::inRange(int row, int col) {     // проверяем находится ли элемент в диапазоне нашей игровой доски
    bool positive = row >= 0 && col >= 0;
	bool upper = row < this->boardSize && col < this->boardSize;
	return positive && upper;
}


QVector<Tile *> Board::freeTiles() {  // установка на место пустующего элемента массива ячейки
	QVector<Tile *> freeTiles;

    foreach (Tile* tile, this->tiles) { // итерация по массиву tiles
        if (tile->isFree()) { // если элемент массива пуст, то вставляем ячейку на его место
			freeTiles << tile;
		}
	}
	return freeTiles;
}


QVector<Tile *> Board::tilesAroundTile(int row, int col) { // создаем массив с элементами вокруг основного элемента
	QVector<Tile *> tilesAround;

    for (int i = -1; i <= 1; i += 2) { // начинаем с -1 и до 1, шаг +2, таким образом смотрим левый/правый, верхний/нижний элементы вокруг ячейки
        Tile* tile = this->tile(row + i, col); // если все хорошо по ряду, а именно элемент находится внутри доски, то
		if (tile) {
            tilesAround << tile;  // добавляем его в массив tilesAround
		}
        tile = this->tile(row, col + i);  // если все хорошо по колонке, а именно элемент находится внутри доски, то
		if (tile) {
            tilesAround << tile;  // добавляем его в массив tilesAround
		}
	}
	return tilesAround;
}


void Board::addRandomTile() {  // добавление в рандомную ячейку
	QVector<Tile *> freeTiles = this->freeTiles();

	if (freeTiles.length() > 0) {
		int randomNumber = QRandomGenerator::global()->bounded(freeTiles.length());
		Tile* randomTile = freeTiles[randomNumber];
		randomTile->setValue(2);
	}
}


bool Board::isMovePossible() {  // проверка есть ли возможность переместить элемент
	for (int r = 0; r < this->boardSize; r++) {
		for (int c = 0; c < this->boardSize; c++) {
			Tile* tile = this->tile(r, c);
			if (tile) {
                if (tile->isFree()) {  // если существует пустой элемент, то перемещение возможно
					return true;
				}
                QVector<Tile *> adjacents = this->tilesAroundTile(r, c); // формируем массив вокруг каждого элемента и проверяем есть ли равные элементы
				foreach (Tile* adjacent, adjacents) {
                    if (tile->equalTo(adjacent)) { // есть есть, то и возможность переместить есть
                        return true;
					}
				}
			}
		}
	}
    return false; // иначе нельзя переместить
}
