#ifndef BOARD_H
#define BOARD_H

#include "tile.h"

#include <QWidget>
#include <QGridLayout>
#include <QVector>
#include <QRandomGenerator>
#include <QMessageBox>
#include <QGraphicsScene>


class Board : public QWidget {
		Q_OBJECT

	public:
		explicit Board(int size, int scale, QWidget *parent = nullptr)
			: QWidget(parent), moved(true), boardSize(size), boardScale(scale) {}
		void setupBoard();
		void resetBoard();
		void moveUp();
		void moveDown();
		void moveLeft();
		void moveRight();

//    signals:
//        void scoreChanged(int);

    private slots:
        void scoreUpdate(int score);

	private:
		bool moved;
		int boardSize;
		int boardScale;
        int score;
        int bestscore;
		QVector<Tile *> tiles;
        QGraphicsScene *scene;
        QLabel *lScore;
        QLabel *lScoreBest;
        QPushButton *pbRestart, *pbQuit;


		Tile* tile(int row, int col);
		QVector<Tile *> freeTiles();
		QVector<Tile *> tilesAroundTile(int row, int col);
		bool inRange(int row, int col);
		bool isMovePossible();
		void moveTile(int row, int col, int rowChange, int colChange);
		void afterMovement();
		void showLoosePopUp();
		void addRandomTile();


};

#endif // BOARD_H
